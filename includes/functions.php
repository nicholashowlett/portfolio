<?php

    /**
     * functions.php
     *
     * portfolio
     * Nicholas Howlett
     *
     * Main helper functions.
     */

    /*************************************************************************************
     * Emails me with user submitted data. Returns true if email sent successfully,
     * otherwise returns false if unsuccessful and triggers warning message.
     */
    function emailMe($name, $email, $message)
    {
        // get local PHPMailer library
        getFileOrError(__DIR__ . "/phpmailer/class.phpmailer");
        getFileOrError(__DIR__ . "/phpmailer/class.smtp");
    
        // instantiate object of class PHPMailer (enable exception use)
        $mail = new PHPMailer(true);

        // attempt the following and throw exception if any errors occur
        try
        {
            // set SMTP settings for TPG
            $mail->IsSMTP();
            $mail->Host = MAIL_SERVER;
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;

            // SMTP Authentication
            $mail->SMTPAuth =- true;
            $mail->Username = USERNAME;
            $mail->Password = PASSWORD;
      
            // set email details
            $mail->SetFrom("$email", $name);
            $mail->AddAddress(MY_EMAIL);
            $mail->Subject = "Contacted via Portfolio";
            $mail->Body = "$message";
    
            // attempt to send email then indicate success
            $mail->Send();
            $sent = true;
        }
        // if exception thrown 
        catch (phpmailerException $e)
        {
            // trigger warning with details then indicate email not sent
            trigger_error("Could not send Email via PHPMailer: " . $e->getMessage(), E_USER_WARNING);
            $sent = false;
        }
        
        // return whether email sent or not
        return $sent;
    }

    /*************************************************************************************
     * Renders template (view) if file exists, otherwise triggers warning message.
     */
    function render($template, $values = [])
    {
        // allow previously declared variable access in template
        global $success;
        global $errors;
        //global $validatedName;
        global $validatedEmail;
        global $validatedMessage;
         
        // template path
        $path = __DIR__ . "/../views/" . $template . ".php";
        
        // if template exists
        if (file_exists($path))
        {
            // extract keys as variables
            extract($values);

            // get template content
            require($path);
        }
        // otherwise trigger warning message
        else
        {
            trigger_error("Could not find " . $path, E_USER_WARNING);
        }
    }
    
    /*************************************************************************************
     * Validates user input by whitelist testing. Returns true if input data is valid, 
     * otherwise returns error message string if not valid.
     */
    function validate($value, $varName)
    {
        // check input not empty
        if ($value != "")
        {
            // for name input
            if ($varName == "name")
            {
                // if input is not only alphabetical characters, apostrophes, spaces 
                // construct invalid error message (whitelist test)
                $valueChecked = preg_match("/[^a-zA-Z'\s]/", $value);
                if ($valueChecked)
                {
                    $error = "Please enter a valid name.";
                }
            }
            // for email input
            else if ($varName == "email")
            {                
                // if input not valid sequence then construct invalid error message 
                // (whitelist test) 
                $valueVal = filter_var($value, FILTER_VALIDATE_EMAIL);
                if ($valueVal === false)
                {
                    $error = "Please enter a valid email address.";
                }
            }            
        }
        // otherwise construct empty error message
        else
        {
            $error = "Please enter your " . $varName . ".";
        }
        
        // if error found return error message, otherwise return true
        if (isset($error))
        {
            return $error;
        }
        else
        {
            return true;
        }
    }
    
?>
