        <!-- landing page -->
        <section class="tab-pane active">
          <div class="jumbotron">
            <h2><?= $xml->home->title ?></h2>
            <p>
              <?= $xml->home->main ?><br><br>
              Nick.
            </p>
          </div>
        </section>
        
        <!-- project mashup -->
        <section id="project-mashup" class="tab-pane"><!-- remove "active" if no screen content wanted -->
          <article>
            <h4><?= $xml->application->title0 ?></h4>
            <p>
              <?= $xml->application->main0 ?>
            </p>
            <p>
              <a href="http://nicholashowlettmashup.tk"><!-- specify -->
                <img class="img" alt="Mashup Image" src="../static/mashup-screenshot-1.png" width="700px" height="350px"/>
              </a>
            </p>
            <p>
              <?= $xml->application->note0 ?>
            </p>
          </article>
        </section>

        <!-- project finance -->        
        <section id="project-finance" class="tab-pane">
          <article>
            <h4><?= $xml->application->title1 ?></h4>
            <p>
              <?= $xml->application->main1 ?>
            </p>
            <p>
              <a href="/finance/"><!-- specify -->
                <img class="img" alt="Finance Image" src="../static/finance-screenshot-1.png" width="700px" height="350px"/>
              </a>
            </p>
            <p>
              <?= $xml->application->note1->zero ?>
            </p>
            <p>
              <?= $xml->application->note1->one ?>
            </p>
          </article>
        </section>

        <!-- about -->
        <section id="about" class="tab-pane">
          <h4><?= $xml->about->title ?></h4>
          <p>
            <?= $xml->about->main ?>
          </p>
          <p>
            <img alt="Cat Image" src="../static/my-cat.jpg" width="375px" height="300px"/>
          </p>
        </section>

        <!-- contact -->
        <section id="contact" class="tab-pane">
          <h4><?= $xml->contact->title ?></h4>
          <!-- my details -->
          <div id="details">
            <p>
              <?= $xml->contact->main ?>
            </p>
          </div>
          <!-- form details -->
          <div id="form">
            <form action="<?php $_PHP_SELF ?>" method="POST">
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <input class="form-control" id="name" type="text" name="name" placeholder="Your name"
<?php
    // repopulate name if validated
    if ($GLOBALS['validatedName'] == true) echo 'value="' . $_POST['name'] . '"';
?>
                    />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-6">
                    <input class="form-control" id="email" type="text" name="email" placeholder="Your email address"
<?php    
    // repopulate email if validated
    if ($validatedEmail == true) echo 'value="' . $_POST["email"] . '"';
?>
                    />
                  </div>
                </div>
              </div>
              <div class="form-group">           
<?php   
    // repopulate message if validated
    if ($validatedMessage == true):
        echo '<textarea class="form-control" id="message" rows="5" name="message" placeholder="Message">' . $_POST["message"] . '</textarea>';
    else:
        echo '<textarea class="form-control" id="message" rows="5" name="message" placeholder="Message"></textarea>';
    endif;
?>
              </div>
              <div class="form-group" id="button">
                <button class="btn btn-default" type="submit">Send</button>
              </div>
            </form>
          </div>
        </section>
