<?php

    /**
     * index.php
     *
     * portfolio
     * Nicholas Howlett
     *
     * Controller for multi-page website that allows user to send a message to a 
     * specified email address. 
     *
     * Includes error logging & emailing, and user-submitted data validation & 
     * repopulation of valid user data.
     */

    // messages displayed to user
    $success = "Your message has been sent!";
    $error_500 = "Sorry, we made an error. Please try again later. Sad face :(";

    /*************************************************************************************
     * Custom error handling. Logs & Emails me specific details of error, when any level
     * of message triggered. Responds to client with user-friendly message (including 
     * status code).
     */
    function errorHandling($errno, $errstr, $file, $line)
    {
        // error details
        $time = strftime("%F %r", time());
        $message = "[" . $time . "] " . $errstr . " in " . $file  . " on line " . $line . ". \n";

        // email and log error
        //mail(MY_EMAIL, "Portfolio Error", $message); // OR error_log($message, 1, MY_EMAIL);
        error_log($message, 0);

        // set response status code
        http_response_code(500);

        // display abstracted server error message to user
        exit($GLOBALS["error_500"]);
    }

    // set errors to be handled by custom error handling function
    set_error_handler("errorHandling");

    /*************************************************************************************
     * Gets file (within includes directory) passed in as parameter. Triggers error 
     * message if cannot find file.
     */
    function getFileOrError($filename)
    {
        $path = __DIR__ . "/../includes/" . $filename . ".php";
        if (file_exists($path))
        {
            require($path);
        }
        else
        {
            trigger_error("Could not find " . $path, E_USER_ERROR);
        }
    }

    // configuration file
    getFileOrError("config");

    // helper functions file
    getFileOrError("functions");

    // checking for repopulation of form user input
    $validatedName = false;
    $validatedEmail = false;
    $validatedMessage = false;

    // render header content
    render("header");
    
    // if form submitted
    if ($_SERVER["REQUEST_METHOD"] === "POST")
    {        
        // for each field of form
        $errors = [];
        foreach ($_POST as $key => $value)
        {
            // create string from form input ID
            $validatedVar = "validated" . ucwords($key);
            
            // validate input data (store error message if not valid) and indicate
            // whether field data valid or not (for repopulation of submitted data)
            $validated = validate($value, $key);
            if ($validated !== true)
            {
                $errors[] = $validated;
                $$validatedVar = false;
            }
            else
            {
                $$validatedVar = true;
            }
        }
               
        // render all error messages (if any)   
        if (count($errors) != 0)
        {
            render("error", ["id" => "error_user"]);
        }
        // if user-submitted data valid
        else
        {
            // attempt to send me email containing user's data then render confirmation if 
            // email successfully sent
            $sent = emailMe($_POST["name"], $_POST["email"], $_POST["message"]);
            if ($sent == true)
            {
                render("confirmation");
            }
        }
    }

    // render 'psuedo-static' content from external (XML) file
    $xml = simplexml_load_file("../includes/descriptions.xml");
    render("main", ["xml" => $xml]);

    // render footer content
    render("footer");

?>
